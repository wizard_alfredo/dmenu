static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#a89984", "#3c3836" },
	[SchemeSel]  = { "#3c3836", "#a89984" },
	[SchemeOut]  = { "#eadecf", "#cc241d" },
};
